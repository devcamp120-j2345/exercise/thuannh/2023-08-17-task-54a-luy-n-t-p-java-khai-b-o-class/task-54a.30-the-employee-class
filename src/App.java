import models.Employee;

public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(1, "John", "Doe", 5000);
        Employee employee2 = new Employee(2, "Jane", "Doe", 6000);

        // In thông tin của 2 đối tượng nhân viên
        System.out.println(employee1.toString());
        System.out.println(employee2.toString());

        // In tên đầy đủ và lương 1 năm của 2 nhân viên
        System.out.println("\nEmployee1: " + employee1.getName());
        System.out.println("Employee1's salary per year: " + employee1.getAnnualSalary());
        System.out.println("\nEmployee2: " + employee2.getName());
        System.out.println("Employee2's salary per year: " + employee2.getAnnualSalary());

        // In lại lương sau khi tăng của 2 nhân viên
        System.out.println("\nEmployee1's salary per year then: " + employee1.raiseSalary(10));
        System.out.println("Employee2's salary per year then: " + employee2.raiseSalary(15));
    }
}
